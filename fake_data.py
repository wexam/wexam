#!/usr/bin/env python3

# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import re
import sys
import shutil
import hashlib
import requests
import datetime
import itertools

FAKE_DATA_TAR_URL = "https://jben.approximatifs.fr/fake_data.3.tar"
FAKE_DATA_TAR_HASH = "848cbe3e3753bdb5ddda1446049cef36a64a71460790c4a1f9920870446af992"

CACHE_TAR = ".cache_fake_data.tar"
CACHE_DIR = "fake_data"


def get_cache_fakedata():
    if not os.path.exists(CACHE_TAR):
        r = requests.get(FAKE_DATA_TAR_URL)
        open(CACHE_TAR, "wb").write(r.content)
        del r
    h = hashlib.sha256()
    h.update(open(CACHE_TAR, "rb").read())
    if h.hexdigest() != FAKE_DATA_TAR_HASH:
        print(
            f"Cached file {CACHE_TAR!r} is invalid. Remove it and run again the script.",
            file=sys.stderr,
        )
        sys.exit(1)
    if os.path.exists(CACHE_DIR):
        shutil.rmtree(CACHE_DIR)
    os.mkdir(CACHE_DIR)
    shutil.unpack_archive(CACHE_TAR)


from wexam import models


def _get_session():
    models.base.metadata.create_all(models.db)
    session = models.Session()
    return session


def insert_data():
    session = _get_session()

    users = {
        "lmichel": models.User(
            login="fakelmichel",
            name="Laurène",
            surname="Michel",
            birthdate=datetime.date(2000, 7, 10),
            cas_role="student",
        ),
        "arheaume": models.User(
            login="fakearheaume",
            name="Adèle",
            surname="Rhéaume",
            birthdate=datetime.date(1942, 1, 24),
            cas_role="student",
        ),
        "ahuppe": models.User(
            login="fakeahuppe",
            name="Agramant",
            surname="Huppé",
            birthdate=datetime.date(2000, 3, 27),
            cas_role="student",
        ),
        "asaindon": models.User(
            login="fakeasaindon",
            name="Alfred",
            surname="Saindon",
            birthdate=datetime.date(1966, 3, 23),
            cas_role="student",
        ),
        "eauclair": models.User(
            login="fakeeauclair",
            name="Émilie",
            surname="Auclair",
            birthdate=datetime.date(1946, 5, 26),
            cas_role="student",
        ),
        "acousteau": models.User(
            login="fakeacousteau",
            name="Alexandre",
            surname="Cousteau",
            birthdate=datetime.date(1987, 10, 14),
            cas_role="student",
        ),
        "prof1": models.User(
            login="prof1", name="Prof1", surname="PROF1", cas_role="teacher"
        ),
        "legerjea": models.User(
            login="legerjea", name="Prof1", surname="PROF1", override_role="admin"
        ),
    }

    sems = {
        "P21": models.Semester(
            code="P21",
            name="Printemps 2021",
            start_date=datetime.date(2021, 2, 16),
            end_date=datetime.date(2021, 8, 31),
        ),
        "A21": models.Semester(
            code="A21",
            name="Automne 2021",
            start_date=datetime.date(2021, 9, 1),
            end_date=datetime.date(2021, 2, 15),
        ),
    }

    courses_base = {
        "CA01": models.Course(code="CA01", name="Castor IPA"),
        "CA03": models.Course(code="CA03", name="Carolus Triple"),
        "CT90": models.Course(code="CT90", name="Cuvée des trolls"),
    }

    courses_A21 = {
        "CA01": models.CourseSemester(
            course=courses_base["CA01"], semester=sems["A21"]
        ),
        "CA03": models.CourseSemester(
            course=courses_base["CA03"], semester=sems["A21"]
        ),
        "CT90": models.CourseSemester(
            course=courses_base["CT90"], semester=sems["A21"]
        ),
    }

    memberships_A21 = {
        (c, u): models.CourseMember(user=users[u], course_semester=courses_A21[c])
        for (c, u) in (
            ("CA03", "lmichel"),
            ("CA03", "arheaume"),
            ("CA03", "ahuppe"),
            ("CA03", "asaindon"),
            ("CT90", "lmichel"),
            ("CT90", "ahuppe"),
            ("CT90", "eauclair"),
            ("CT90", "acousteau"),
            ("CT90", "prof1"),
            ("CA03", "prof1"),
        )
    }

    exams = {
        ("CA03", 1): models.Exam(
            course_semester=courses_A21["CA03"],
            name="Exam1",
            slug="exam1",
        ),
        ("CA03", 2): models.Exam(
            course_semester=courses_A21["CA03"],
            name="Exam2",
            slug="exam2",
        ),
        ("CT90", 1): models.Exam(
            course_semester=courses_A21["CT90"],
            name="Exam1",
            slug="exam1",
        ),
    }

    parts = {
        (c, n, p): models.ExamPart(
            exam=exams[c, n], name=f"Part{p}", position_in_exam=p
        )
        for (c, n, p) in (
            ("CA03", 1, 1),
            ("CA03", 2, 1),
            ("CA03", 2, 2),
            ("CT90", 1, 1),
            ("CT90", 1, 2),
        )
    }

    n_grps_CA03 = set(
        x.split("-")[0] for x in os.listdir("fake_data/CA03") if re.match(r".*\.jpg", x)
    )
    all_pages = []
    for part in (
        parts[x]
        for x in (
            ("CA03", 1, 1),
            ("CA03", 2, 1),
            ("CA03", 2, 2),
        )
    ):
        for k in n_grps_CA03:
            grp = models.GroupOfPages(
                exam_part=part,
                format=0,
            )
            all_pages.extend(
                models.Page(
                    content=open(f"fake_data/CA03/{k}-{i}.jpg", "rb").read(),
                    position_in_group=i,
                    group_of_pages=grp,
                    orientation="0",
                )
                for i in range(4)
            )

    exam_CT90_papers = {
        (who, part): models.PartPaper(
            student=users[who],
            exam_part=parts["CT90", 1, part],
            grade=grade,
            published=True,
            comment=comment,
        )
        for (who, grade, comment) in (
            ("lmichel", 18, "Very good"),
            ("ahuppe", 17, None),
            ("eauclair", 9, None),
            ("acousteau", 9, "Very bad"),
        )
        for part in (1, 2)
    }

    def build_CT90_group(who, part, orientation, k, order, pos):
        grp = models.GroupOfPages(
            exam_part=parts["CT90", 1, part],
            part_paper=exam_CT90_papers[who, part],
            position_in_part=pos,
            format=0,
        )
        all_pages.extend(
            models.Page(
                content=open(f"fake_data/CT90/{k}-{i}.jpg", "rb").read(),
                position_in_group=o,
                group_of_pages=grp,
                orientation=orientation,
            )
            for i, o in enumerate(order)
        )

    for part in (1, 2):
        build_CT90_group("lmichel", part, "0", "01", [0, 1, 2, 3], 1)
        build_CT90_group("lmichel", part, "0", "00", [0, 1, 2, 3], 0)
        build_CT90_group("ahuppe", part, "0", "02", [0, 1, 2, 3], 0)
        build_CT90_group("ahuppe", part, "0", "03", [2, 3, 0, 1], 1)
        build_CT90_group("eauclair", part, "180", "05", [3, 2, 1, 0], 1)
        build_CT90_group("eauclair", part, "0", "04", [0, 1, 2, 3], 0)
        build_CT90_group("acousteau", part, "0", "06", [0, 1, 2, 3], 0)
        build_CT90_group("acousteau", part, "180", "07", [1, 0, 3, 2], 1)

    for obj in itertools.chain(
        users.values(),
        sems.values(),
        courses_base.values(),
        courses_A21.values(),
        memberships_A21.values(),
        exams.values(),
        parts.values(),
        all_pages,
    ):
        session.add(obj)
    session.commit()


if __name__ == "__main__":
    get_cache_fakedata()
    insert_data()
