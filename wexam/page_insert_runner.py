# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import time

import sqlalchemy.orm.session
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

import wexam_pdfimg as pdfimg

from . import models

TIME_SLEEP = int(os.getenv("PAGE_INSERT_RUNNER_TIME_SLEEP", 1))


def _vacuum_analyze():
    conn = models.db.raw_connection()
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    cursor.execute("VACUUM ANALYSE;")


def _error(session, job, msg):
    job.status = models.InsertionStatus.error
    job.errormsg = msg
    session.query(models.InsertionJobFile).filter(
        models.InsertionJobFile.job == job
    ).delete()
    session.commit()


def insert_pages_from_pdf(session: sqlalchemy.orm.session.Session, job_id: int):
    job = (
        session.query(models.InsertionJob)
        .filter(models.InsertionJob.id == job_id)
        .first()
    )
    if job.format < 0:
        _error(session, job, "Invalid paper format.")
        return None
    if not job.job_file:
        _error(session, job, "Missing file.")
        return None

    try:
        if job.format == 0:
            pages_groups = pdfimg.A3_double(job.job_file[0].content)
        else:
            pages_groups = pdfimg.A4(job.job_file[0].content, ppg=job.format)
    except (pdfimg.PdfReadError, pdfimg.PdfNumberOfPagesError) as e:
        if isinstance(e, pdfimg.PdfNumberOfPagesError):
            _error(session, job, "Invalid number of pages.")
        else:
            _error(session, job, "Invalid pdf file.")
        return None

    job.no_pages_total = len(pages_groups)
    job.no_pages_inserted = 0
    job.status = models.InsertionStatus.running
    session.commit()
    for i, pages_group in enumerate(pages_groups):
        grp = models.GroupOfPages(
            id=-(2**31) + 1024 * job.id + i,
            exam_part=job.exam_part,
            format=job.format,
        )
        for i, p in enumerate(pages_group):
            session.add(
                models.Page(
                    content=p.content,
                    position_in_group=i,
                    group_of_pages=grp,
                    orientation="0",
                )
            )
        job.no_pages_inserted += 1
        session.commit()
    job.status = models.InsertionStatus.done
    session.query(models.InsertionJobFile).filter(
        models.InsertionJobFile.job == job
    ).delete()
    session.commit()


def poll_a_job(session: sqlalchemy.orm.session.Session):
    try:
        with session.begin():
            job = (
                session.query(models.InsertionJob)
                .filter(models.InsertionJob.status == models.InsertionStatus.waiting)
                .order_by(models.InsertionJob.id)
                .first()
            )
            if job is None:
                return None
            job.status = models.InsertionStatus.starting
            print(f"Try: {job.id}")
    except models.OperationalError:
        session.rollback()
        print(f"Fail.")
        # concurency fail between runner, another runner took the job
        return None
    print(f"Ok.")
    return job.id


def main_loop():
    while True:
        with models.Session(bind=models.db_serializable) as session:
            job_id = poll_a_job(session)
        if job_id is not None:
            with models.Session() as session:
                insert_pages_from_pdf(session, job_id)
            _vacuum_analyze()
        else:
            time.sleep(TIME_SLEEP)
