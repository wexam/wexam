# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from flask import Blueprint, render_template, redirect, request, url_for

from . import auth as fake_auth
from .. import auth, models, router


def build_fake_cas():
    fake_cas = Blueprint(
        "fake_cas", __name__, static_folder="static", url_prefix="/cas"
    )

    @fake_cas.route("/login", methods=["GET", "POST"])
    def login():
        if request.method == "POST":
            role = request.args.get("role")
            return fake_auth.fake_login(request.form, role)
        else:
            with models.Session() as session:
                students = [
                    stud
                    for stud in session.query(models.User)
                    .filter(
                        (models.User.cas_role.is_(None))
                        | (models.User.cas_role == models.Role.student)
                    )
                    .all()
                    if stud.courses_memberships
                ]
                students.sort(
                    key=lambda stud: (
                        router._helpers.normalize_str(
                            stud.courses_memberships[0].surname
                        ),
                        router._helpers.normalize_str(stud.courses_memberships[0].name),
                    )
                )
            return render_template("./fake_cas.html", students=students)

    @fake_cas.route("/logout", methods=["GET"])
    def logout(user=None, session=None):
        fake_auth.fake_logout(session)
        return redirect(url_for("cas.login"))

    @fake_cas.route("/course/new", methods=["POST"])
    @auth.login_required
    def create_fake_course(user=None, session=None):
        course_code = request.form["code"]
        course_name = request.form["name"]

        with session.begin():
            router.admin._helper_course_new(
                session=session, course_code=course_code, course_name=course_name
            )

            semester = (
                session.query(models.Semester)
                .order_by(models.Semester.start_date.desc())
                .first()
            )
            router.admin._helper_course_semester_new(
                session=session, semester_code=semester.code, course_code=course_code
            )

            course_semester, _ = auth.get_course_semester_and_membership(
                session=session,
                user=user,
                semester_code=semester.code,
                course_code=course_code,
            )
            router.course._add_user_to_course(
                session, user, course_semester, user.login, role=models.Role.teacher
            )

        return redirect(
            url_for(
                "main.view",
            )
        )

    return fake_cas
