# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
from flask import Flask

# begin of dirty
# Monkey patch. flask-cas-ng have to stop to use this deprecated API
import flask

if not hasattr(flask, "_app_ctx_stack"):
    import collections

    flask._app_ctx_stack = collections.namedtuple("DummyContext", ("top",))(None)
# end of dirty
from flask_cas import CAS

from .models import db, base, update_schema_if_needed
from . import router

if (load_fake_cas := os.getenv("FAKE_CAS", None)) is not None and load_fake_cas == "1":
    from . import fake_auth


def create_app():
    app = Flask(__name__)
    app.config["SECRET_KEY"] = os.getenv(
        "WEXAM_FLASK_SECRET_KEY", "9OLWxND4o83j4K4iuopO"
    )

    CAS(app)
    app.config["CAS_SERVER"] = os.getenv("WEXAM_CAS_SERVER_URL")
    app.config["CAS_AFTER_LOGIN"] = "main.index"

    app.register_blueprint(router.main.main)
    app.register_blueprint(router.admin.admin)
    app.register_blueprint(router.course.course)
    app.register_blueprint(router.exam.exam)
    app.register_blueprint(router.user.user)
    app.register_blueprint(router.render.render)

    if (
        load_fake_cas := os.getenv("FAKE_CAS", None)
    ) is not None and load_fake_cas == "1":
        app.register_blueprint(fake_auth.routing.build_fake_cas())

    app.register_error_handler(403, router.errors.page_forbidden)

    base.metadata.create_all(db)
    update_schema_if_needed()

    @app.template_filter()
    def env(key):
        return os.getenv(key, None)

    return app


def run_app():
    app = create_app()
    app.run(debug=True)
