# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import json
import datetime
import itertools
from dataclasses import dataclass

from flask import (
    Blueprint,
    Response,
    redirect,
    render_template,
    request,
    abort,
    url_for,
)
from sqlalchemy import null

from .. import auth
from .. import models
from .. import auth
from ..auth import login_required
from . import _exam_helpers
from . import _helpers

exam = Blueprint(
    "exam",
    __name__,
    url_prefix="/exam",
    template_folder="templates",
    static_folder="static",
)


@exam.route("/<semester_code>/<course_code>/new", methods=["POST"])
@login_required
def create_exam(semester_code, course_code, user=None, session=None):
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = models.Exam(
            name=request.form["exam_name"],
            slug=models.slugify(request.form["exam_name"]),
            date=request.form["exam_date"],
            course_semester=course_semester,
        )

        session.add(exam)
        _helpers.log_exam_action(session, user, exam, "Create exam")

        for i, part_name in enumerate(request.form.getlist("part_name")):
            part = models.ExamPart(
                course_code=course_code,
                semester_code=semester_code,
                exam=exam,
                name=part_name,
                position_in_exam=i,
            )
            session.add(part)
            _helpers.log_part_action(session, user, part, "Create part")

    return redirect(url_for("main.view"))


@exam.route("/<semester_code>/<course_code>/<exam_slug>/delete", methods=["GET"])
@login_required
def delete_exam(semester_code, course_code, exam_slug, user=None, session=None):
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )

        exam_parts = (
            session.query(models.ExamPart).filter(models.ExamPart.exam == exam).all()
        )

        pages = (
            session.query(models.Page)
            .join(models.GroupOfPages)
            .join(models.ExamPart)
            .filter(models.ExamPart.exam == exam)
            .all()
        )

        groups_of_pages = (
            session.query(models.GroupOfPages)
            .join(models.ExamPart)
            .filter(models.ExamPart.exam == exam)
            .all()
        )

        part_papers = (
            session.query(models.PartPaper)
            .join(models.ExamPart)
            .filter(models.ExamPart.exam == exam)
            .all()
        )

        insertion_jobs_files = (
            session.query(models.InsertionJobFile)
            .join(models.InsertionJob)
            .join(models.ExamPart)
            .filter(models.ExamPart.exam == exam)
            .all()
        )

        insertion_jobs = (
            session.query(models.InsertionJob)
            .join(models.ExamPart)
            .filter(models.ExamPart.exam == exam)
            .all()
        )

        if any(ep.status_insertion_waiting_or_running for ep in exam_parts):
            abort(409)

        for elem in itertools.chain(
            insertion_jobs_files,
            insertion_jobs,
            pages,
            groups_of_pages,
            part_papers,
            exam_parts,
            (exam,),
        ):
            session.delete(elem)
        _helpers.log_course_semester_action(
            session, user, course_semester, "Delete exam"
        )

        return redirect(url_for("main.view"))


@dataclass
class ZipFile:
    url: str
    name: str


@exam.route("/<semester_code>/<course_code>/<exam_slug>/table", methods=["GET"])
@auth.login_required
def table_exam(semester_code, course_code, exam_slug, user=None, session=None):
    details = True if request.args.get("details") == "true" else False
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        _helpers.log_exam_action(session, user, exam, "View exam table")

        students, exam_results = _exam_helpers.get_exam_results(
            session, exam, course_semester, papers=True, details=details
        )
        columns = tuple(exam_results.keys())

        zipfile = ZipFile(
            url=f"/render/pdfzip/{semester_code}_{course_code}_{exam_slug}.zip",
            name=f"{exam.name}",
        )

        students.sort(
            key=lambda x: (
                _exam_helpers.sort_key_literal(x.surname),
                _exam_helpers.sort_key_literal(x.name),
                x.login,
            ),
        )

        orders = {
            "Nom": _exam_helpers.revargsort(
                students,
                key=lambda x: (
                    _exam_helpers.sort_key_literal(x.surname),
                    _exam_helpers.sort_key_literal(x.name),
                    x.login,
                ),
            ),
            "Prénom": _exam_helpers.revargsort(
                students,
                key=lambda x: (
                    _exam_helpers.sort_key_literal(x.name),
                    _exam_helpers.sort_key_literal(x.surname),
                    x.login,
                ),
            ),
        }

        for col in columns:
            orders[col] = _exam_helpers.revargsort(
                students,
                key=lambda x: (
                    _exam_helpers.sort_key_number(exam_results[col][x.login].grade),
                    x.login,
                ),
            )

    return render_template(
        "table.html",
        user=user,
        students=students,
        exam=exam,
        columns=columns,
        exam_results=exam_results,
        tablebasename=f"{semester_code}_{course_code}_{exam_slug}",
        zips=(zipfile,),
        orders=orders,
        details=details,
    )


@exam.route("/<semester_code>/<course_code>/<exam_slug>/manage", methods=["GET"])
@auth.login_required
def manage_exam(semester_code, course_code, exam_slug, user=None, session=None):
    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        _helpers.log_exam_action(session, user, exam, "Manage exam")

        return render_template(
            "exam_manage.html",
            user=user,
            exam=exam,
            status=_helpers.STATUS_FRONT,
        )


@exam.route("/rawupload/<int:part_id>", methods=["POST"])
@auth.login_required
def rawupload(part_id, user=None, session=None):
    uploaded_pages = request.files.getlist("page")
    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        student_login = request.form["login"]
        student_grade = request.form["grade"]
        student_pages = [p.read() for p in uploaded_pages]

        student = (
            session.query(models.User)
            .join(models.CourseMember)
            .filter(models.CourseMember.course_semester == part.exam.course_semester)
            .filter(models.User.login == student_login)
            .first()
        )

        if student is None:
            return Response(
                json.dumps({"status": "FAIL", "msg": "Unknown student"}),
                mimetype="application/json",
            )

        part_paper = (
            session.query(models.PartPaper)
            .filter(models.PartPaper.exam_part == part)
            .filter(models.PartPaper.student == student)
            .first()
        )

        groups_of_pages = [
            models.GroupOfPages(
                format=len(student_pages),
                exam_part=part,
                pages=[
                    models.Page(content=content, orientation=0, position_in_group=i)
                    for i, content in enumerate(student_pages)
                ],
            )
        ]

        new = True
        if part_paper is not None:
            for gp in part_paper.groups_of_pages:
                for p in gp.pages:
                    session.delete(p)
                session.delete(gp)
            new = False
            part_paper.groups_of_pages = groups_of_pages
            part_paper.grade = student_grade
            part_paper.published = False
        else:
            part_paper = models.PartPaper(
                student=student,
                grade=student_grade,
                published=False,
                exam_part=part,
                groups_of_pages=groups_of_pages,
            )

        session.add(part_paper)
        _helpers.log_partpaper_action(
            session, user, part_paper, "Raw upload partpaper", target_user=student
        )
    return Response(
        json.dumps(
            {
                "status": "ok",
                "msg": "New part paper" if new else "Replace existing part paper",
            }
        ),
        mimetype="application/json",
    )


@exam.route("/upload/<int:part_id>", methods=["POST"])
@auth.login_required
def upload(part_id, user=None, session=None):
    uploaded_files = request.files.getlist("upload_file")
    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        if request.form["format"] == "A3_double":
            format = 0
        else:
            format = int(request.form["ppg"])
        if format < 0 or format > 20:
            abort(422)
        for uploaded_file in uploaded_files:
            job = models.InsertionJob(
                exam_part=part,
                status=models.InsertionStatus.waiting,
                datetime=datetime.datetime.now(),
                filename=uploaded_file.filename,
                format=format,
            )
            jobfile = models.InsertionJobFile(job=job, content=uploaded_file.read())
            session.add(jobfile)

            _helpers.log_insertionjob_action(session, user, job, "Create insersion job")

    return redirect(
        url_for(
            "exam.manage_exam",
            semester_code=part.exam.course_semester.semester.code,
            course_code=part.exam.course_semester.course.code,
            exam_slug=part.exam.slug,
        )
    )


@exam.route("/progress/<semester_code>/<course_code>/<exam_slug>", methods=["GET"])
@auth.login_required
def monitor_progress(semester_code, course_code, exam_slug, user=None, session=None):
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        uploads = (
            session.query(models.InsertionJob)
            .join(models.ExamPart)
            .filter(models.ExamPart.exam == exam)
            .all()
        )

    progress = {
        "parts": [
            {
                "id": part.id,
                "number_of_gp": part.nb_groups,
                "status_assoc": _helpers.STATUS_FRONT[part.status[0]],
                "insertion": part.status_insertion_waiting_or_running,
            }
            for part in exam.exam_parts
        ],
        "uploads": [
            {
                "id": upload.id,
                "progress": upload.no_pages_inserted,
                "total": upload.no_pages_total,
                "status": upload.status.display,
                "errormsg": upload.errormsg,
            }
            for upload in uploads
        ],
    }

    return Response(json.dumps(progress), status=200, mimetype="application/json")


def _get_groups(gps_pps_ps):
    for _, groupped_gps_pps_ps in itertools.groupby(gps_pps_ps, key=lambda x: x[0].id):
        groupped_gps_pps_ps = list(groupped_gps_pps_ps)
        pages = [x[2] for x in groupped_gps_pps_ps]
        pages.sort(key=lambda p: p.position_in_group)
        yield (
            groupped_gps_pps_ps[0][0],
            groupped_gps_pps_ps[0][1],
            pages,
        )


@exam.route("/associate/<int:part_id>", methods=["GET"])
@auth.login_required
def associate(part_id, user=None, session=None):
    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        _helpers.log_part_action(session, user, part, "Associate part")

        gps_pps_ps = (
            session.query(models.GroupOfPages, models.PartPaper, models.Page)
            .join(models.PartPaper, isouter=True)
            .join(models.Page)
            .filter(models.GroupOfPages.exam_part == part)
            .order_by(models.GroupOfPages.id)
            .all()
        )

        js_groups_of_pages = {
            f"{gp.id}": {
                "pages": [
                    {"id": p.id, "orientation": str(p.orientation)} for p in pages
                ],
                "student": (pp.student_login if pp is not None else "null"),
                "format": gp.format,
                "dirty": 0,
            }
            for gp, pp, pages in _get_groups(gps_pps_ps)
        }

        course_members = (
            session.query(models.CourseMember)
            .filter(models.CourseMember.course_semester == part.exam.course_semester)
            .all()
        )
        course_members = [u for u in course_members if u.role == models.Role.student]
        course_members.sort(
            key=lambda u: (
                _exam_helpers.sort_key_literal(u.surname),
                _exam_helpers.sort_key_literal(u.name),
                u.birthdate,
            )
        )

        js_student_order = [cm.user.login for cm in course_members]

        part_papers = (
            session.query(models.PartPaper)
            .filter(models.PartPaper.exam_part == part)
            .all()
        )

        js_students = {
            cm.user.login: {
                "name": cm.name,
                "surname": cm.surname,
                "birthdate": cm.birthdate.strftime("%d/%m/%Y"),
                "grade": "null",
                "comment": "",
                "published": "false",
                "groups": list(),
                "dirty": 0,
            }
            for cm in course_members
        }
        for pp in part_papers:
            js_students[pp.student_login]["grade"] = (
                pp.grade if pp.grade is not None else "null"
            )
            js_students[pp.student_login]["comment"] = (
                pp.comment if pp.comment is not None else "null"
            )
            js_students[pp.student_login]["published"] = (
                "true" if pp.published else "false"
            )
            js_students[pp.student_login]["groups"] = [
                gp.id for gp in pp.groups_of_pages
            ]

        return render_template(
            "association.html",
            user=user,
            students=js_students,
            part=part,
            student_order=js_student_order,
            pages=js_groups_of_pages,
        )


class InvalidDataError(Exception):
    pass


@exam.route("/associate/update/<int:part_id>", methods=["POST"])
@auth.login_required
def associate_update(part_id, user=None, session=None):
    content_type = request.headers.get("Content-Type")
    if content_type == "application/json":
        data = request.json
    else:
        abort(406)

    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        try:
            part_papers = {}
            for login, student in data["students"].items():
                current_user = (
                    session.query(models.CourseMember)
                    .filter(
                        models.CourseMember.course_semester == part.exam.course_semester
                    )
                    .filter(models.CourseMember.login == login)
                    .first()
                )
                if current_user is None:
                    raise InvalidDataError

                if (
                    student["grade"] is not None
                    and student["grade"]
                    and student["grade"] != "null"
                ):
                    grade = float(student["grade"])
                else:
                    grade = None

                if (
                    student["comment"] is not None
                    and student["comment"]
                    and student["comment"] != "null"
                ):
                    comment = str(student["comment"])
                else:
                    comment = None

                if (
                    student["published"] is not None
                    and student["published"]
                    and student["published"] != "null"
                ):
                    published = (
                        student["published"] == "true" or student["published"] is True
                    )
                else:
                    published = None

                if grade is not None or comment is not None or published is not None:
                    current_part_paper = (
                        session.query(models.PartPaper)
                        .filter(models.PartPaper.exam_part == part)
                        .filter(models.PartPaper.student == current_user)
                        .first()
                    )
                    if current_part_paper is None:
                        current_part_paper = models.PartPaper(
                            grade=grade,
                            comment=comment,
                            published=published,
                            exam_part=part,
                            student=current_user.user,
                        )
                        session.add(current_part_paper)
                    else:
                        if grade != current_part_paper.grade:
                            current_part_paper.grade = grade
                        if comment != current_part_paper.comment:
                            current_part_paper.comment = comment
                        if published != current_part_paper.published:
                            current_part_paper.published = published
                    part_papers[login] = current_part_paper

            for gpid, gp in data["groups_of_pages"].items():
                current_group_of_pages = (
                    session.query(models.GroupOfPages)
                    .filter(models.GroupOfPages.exam_part == part)
                    .filter(models.GroupOfPages.id == int(gpid))
                    .first()
                )
                if current_group_of_pages is None:
                    raise InvalidDataError

                if (
                    gp["student"] is not None
                    and gp["student"]
                    and gp["student"] != "null"
                ):
                    if gp["student"] in part_papers:
                        current_part_paper = part_papers[gp["student"]]
                    else:
                        current_user = (
                            session.query(models.CourseMember)
                            .filter(
                                models.CourseMember.course_semester
                                == part.exam.course_semester
                            )
                            .filter(models.CourseMember.login == gp["student"])
                            .first()
                        )
                        if current_user is None:
                            raise InvalidDataError
                        current_part_paper = (
                            session.query(models.PartPaper)
                            .filter(models.PartPaper.exam_part == part)
                            .filter(models.PartPaper.student == current_user)
                            .first()
                        )
                        if current_part_paper is None:
                            current_part_paper = models.PartPaper(
                                exam_part=part, student=current_user.user
                            )
                            session.add(current_part_paper)
                    if current_group_of_pages.part_paper is not current_part_paper:
                        current_group_of_pages.part_paper = current_part_paper
                else:
                    current_group_of_pages.part_paper_id = None

                dbpages = {
                    page.id: page
                    for page in session.query(models.Page)
                    .join(models.GroupOfPages)
                    .filter(models.GroupOfPages.id == int(gpid))
                    .all()
                }

                for i, p in enumerate(gp["pages"]):
                    if p["id"] not in dbpages:
                        raise InvalidDataError
                    current_page = dbpages[p["id"]]
                    if str(p["orientation"]) not in ("0", "90", "180", "270"):
                        raise InvalidDataError
                    if str(p["orientation"]) != current_page.orientation:
                        current_page.orientation = str(p["orientation"])
                    if current_page.position_in_group != i:
                        current_page.position_in_group = i
        except Exception as e:
            raise e  # for test only after, should be a abort(409)

        empty_part_papers = (
            session.query(models.PartPaper)
            .join(models.GroupOfPages, isouter=True)
            .filter(models.GroupOfPages.id.is_(None))
            .all()
        )
        for pp in empty_part_papers:
            session.delete(pp)

    return Response(json.dumps({"status": "ok"}), mimetype="application/json")


@exam.route("/delete/<int:part_id>/<gp_id>", methods=["GET"])
@auth.login_required
def delete(part_id, gp_id, user=None, session=None):
    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        gp = (
            session.query(models.GroupOfPages)
            .filter(models.GroupOfPages.id == gp_id)
            .filter(models.GroupOfPages.exam_part == part)
            .first()
        )

        _helpers.log_part_action(session, user, part, "Delete GroupOfPages")

        if not gp:
            abort(404)

        for page in gp.pages:
            session.delete(page)

        session.delete(gp)

        empty_part_papers = (
            session.query(models.PartPaper)
            .join(models.GroupOfPages, isouter=True)
            .filter(models.GroupOfPages.id.is_(None))
            .all()
        )
        for pp in empty_part_papers:
            session.delete(pp)

    return redirect(
        url_for(
            "exam.associate",
            part_id=part.id,
            user=user,
            session=session,
        )
    )


@exam.route("/publish/<int:part_id>", methods=["POST"])
@auth.login_required
def publish_part(part_id, user=None, session=None):
    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        _helpers.log_part_action(session, user, part, "Publish part")

        part_papers = (
            session.query(models.PartPaper)
            .filter(models.PartPaper.exam_part == part)
            .all()
        )

        for pp in part_papers:
            pp.published = True

    return redirect(
        url_for(
            "exam.manage_exam",
            semester_code=part.exam.course_semester.semester.code,
            course_code=part.exam.course_semester.course.code,
            exam_slug=part.exam.slug,
        )
    )


@exam.route("/unpublish/<int:part_id>", methods=["POST"])
@auth.login_required
def unpublish_part(part_id, user=None, session=None):
    with session.begin():
        part, _ = auth.teacher_or_admin_from_partid(session, user, part_id)

        _helpers.log_part_action(session, user, part, "Unpublish part")

        part_papers = (
            session.query(models.PartPaper)
            .filter(models.PartPaper.exam_part == part)
            .all()
        )

        for pp in part_papers:
            pp.published = False

    return redirect(
        url_for(
            "exam.manage_exam",
            semester_code=part.exam.course_semester.semester.code,
            course_code=part.exam.course_semester.course.code,
            exam_slug=part.exam.slug,
        )
    )


@exam.route(
    "/<semester_code>/<course_code>/<exam_slug>/update_correction", methods=["POST"]
)
@auth.login_required
def update_correction(semester_code, course_code, exam_slug, user=None, session=None):
    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        _helpers.log_exam_action(session, user, exam, "Update correction exam")

        exam.correction = request.form["exam_correction"]

    return redirect(
        url_for(
            "exam.manage_exam",
            semester_code=exam.course_semester.semester.code,
            course_code=exam.course_semester.course.code,
            exam_slug=exam.slug,
        )
    )


@exam.route(
    "/<semester_code>/<course_code>/<exam_slug>/update_message", methods=["POST"]
)
@auth.login_required
def update_message(semester_code, course_code, exam_slug, user=None, session=None):
    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        _helpers.log_exam_action(session, user, exam, "Update message exam")

        exam.message = request.form["exam_message"]

    return redirect(
        url_for(
            "exam.manage_exam",
            semester_code=exam.course_semester.semester.code,
            course_code=exam.course_semester.course.code,
            exam_slug=exam.slug,
        )
    )
