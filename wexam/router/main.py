# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from flask import Blueprint, render_template, abort, request

from ..auth import login_required
from .. import models
from . import _helpers

main = Blueprint("main", __name__, template_folder="templates", static_folder="static")


@main.route("/<semester>", methods=["GET"])
@main.route("/")
@login_required
def view(semester=None, user=None, session=None):
    with session.begin():
        semesters = (
            session.query(models.Semester)
            .order_by(models.Semester.start_date.desc())
            .all()
        )

        _helpers.log_user_action(session, user, "Homepage")

        if semesters:
            if semester is None:
                semester = semesters[0]
            else:
                semester = (
                    session.query(models.Semester)
                    .filter(models.Semester.code == semester)
                    .first()
                )

                if semester is None:
                    abort(404)

            courses_semesters_membership = (
                session.query(models.CourseSemester, models.CourseMember)
                .filter(
                    models.CourseSemester.course_code == models.CourseMember.course_code
                )
                .filter(
                    models.CourseSemester.semester_code
                    == models.CourseMember.semester_code
                )
                .filter(models.CourseMember.user == user)
                .filter(models.CourseSemester.semester == semester)
                .order_by(models.CourseSemester.course_code)
                .all()
            )
        else:
            courses_semesters_membership = []

    return render_template(
        "home.html",
        user=user,
        courses_semesters_membership=courses_semesters_membership,
        semesters=semesters,
        active=semester,
        status=_helpers.STATUS_FRONT,
        urlprefix="",
    )


@main.route("/docs/", methods=["GET"])
@login_required
def doc(user=None, session=None):
    _helpers.log_user_action(session, user, "Docs")
    return render_template(
        "docs.html",
        user=user,
        status=_helpers.STATUS_FRONT,
    )
