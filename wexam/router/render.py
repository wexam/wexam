# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import io
import csv
import zipfile
import datetime

import openpyxl
from flask import Blueprint, render_template, request, abort, make_response, Response

import wexam_pdfimg as pdfimg

from .. import models
from .. import auth
from . import _exam_helpers
from . import _helpers

render = Blueprint(
    "render",
    __name__,
    url_prefix="/render",
    template_folder="templates",
    static_folder="static",
)


@render.route("/images/<int:page_id>", methods=["GET"])
@auth.login_required
def image_jpg(page_id, user=None, session=None):
    with session.begin():
        page = session.query(models.Page).filter(models.Page.id == page_id).first()
        if page is None:
            abort(404)
        course_semester = page.group_of_pages.exam_part.exam.course_semester
        membership = (
            session.query(models.CourseMember)
            .filter(models.CourseMember.course_semester == course_semester)
            .filter(models.CourseMember.user == user)
            .first()
        )
        if user.role != models.Role.admin:
            if not membership:
                _helpers.log_part_forbidden(
                    session, user, page.group_of_pages.exam_part
                )
                abort(403)
            if membership.role != models.Role.teacher:
                if page.group_of_pages.part_paper is None:  # non associated paper
                    _helpers.log_part_forbidden(
                        session, user, page.group_of_pages.exam_part
                    )
                    abort(403)
                if not user is page.group_of_pages.part_paper.student:
                    _helpers.log_part_forbidden(
                        session, user, page.group_of_pages.exam_part
                    )
                    abort(403)
                if not page.group_of_pages.part_paper.published:
                    _helpers.log_part_forbidden(
                        session, user, page.group_of_pages.exam_part
                    )
                    abort(403)

        log = models.Log(
            datetime=datetime.datetime.now(),
            origin_user=user,
            impacted_user=page.group_of_pages.part_paper.student
            if page.group_of_pages.part_paper is not None
            else None,
            impacted_course=course_semester.course,
            impacted_semester=course_semester.semester,
            impacted_course_semester=course_semester,
            impacted_exam=page.group_of_pages.exam_part.exam,
            impacted_exam_part=page.group_of_pages.exam_part,
            impacted_group_of_pages=page.group_of_pages,
            impacted_page=page,
            impacted_part_paper=page.group_of_pages.part_paper
            if page.group_of_pages.part_paper is not None
            else None,
            action="Get image",
        )
        session.add(log)

        response = make_response(page.content)
        if page.content[:4] == b"\x89PNG":
            response.headers.set("Content-Type", "image/png")
        else:
            response.headers.set("Content-Type", "image/jpeg")
        response.cache_control.max_age = 86400 * 7 * 13  # 13 weeks
    return response


def _build_one_pdf(session, exam, login, check_published=True):
    pages = (
        session.query(models.Page)
        .join(models.GroupOfPages)
        .join(models.PartPaper)
        .join(models.ExamPart)
        .filter(models.ExamPart.exam == exam)
        .filter(models.PartPaper.student_login == login)
        .filter(models.PartPaper.published if check_published else True)
        .order_by(
            models.ExamPart.position_in_exam,
            models.ExamPart.id,
            models.GroupOfPages.position_in_part,
            models.GroupOfPages.id,
            models.Page.position_in_group,
            models.Page.id,
        )
        .all()
    )

    if not pages:
        return None

    jpegpages = [
        pdfimg.JpegPage(content=page.content, orientation=int(page.orientation.value))
        for page in pages
    ]

    return pdfimg.blobs_to_pdffile(jpegpages)


@render.route(
    "/pdf/<semester_code>_<course_code>_<exam_slug>_<login>.pdf", methods=["GET"]
)
@auth.login_required
def build_pdf(semester_code, course_code, exam_slug, login, user=None, session=None):
    with session.begin():
        course_semester, membership = auth.get_course_semester_and_membership(
            session, user, semester_code, course_code
        )
        if user.role != models.Role.admin:
            if membership is None:
                _helpers.log_course_semester_forbidden(session, user, course_semester)
                abort(403)
            if membership.role != models.Role.teacher:
                if user.login != login:
                    _helpers.log_course_semester_forbidden(
                        session, user, course_semester
                    )
                    abort(403)
                check_published = True
            else:
                check_published = False
        else:
            check_published = False

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        pdffile = _build_one_pdf(session, exam, login, check_published)
        if pdffile is None:
            abort(404)

        log = models.Log(
            datetime=datetime.datetime.now(),
            origin_user=user,
            impacted_user_login=login,
            impacted_course=course_semester.course,
            impacted_semester=course_semester.semester,
            impacted_course_semester=course_semester,
            impacted_exam=exam,
            action="Get pdf",
        )
        session.add(log)

        response = make_response(pdffile)
        response.headers.set("Content-Type", "application/pdf")
        response.cache_control.max_age = 3600  # 1 hour
    return response


class _Buffer:
    def __init__(self):
        self._buf = io.BytesIO()

    def write(self, data):
        return self._buf.write(data)

    def getvalue(self):
        val = self._buf.getvalue()
        self._buf = io.BytesIO()
        return val

    def flush(self):
        return 0


def gen_pdfzip(semester_code, course_code, exam_slug, session):
    with session.begin():
        exam = (
            session.query(models.Exam)
            .filter(models.Exam.slug == exam_slug)
            .filter(models.Exam.course_code == course_code)
            .filter(models.Exam.semester_code == semester_code)
            .first()
        )
        if exam is None:
            abort(404)

        students = (
            session.query(models.User)
            .join(models.CourseMember)
            .filter(models.CourseMember.course_code == course_code)
            .filter(models.CourseMember.semester_code == semester_code)
            .all()
        )

        output = _Buffer()
        with zipfile.ZipFile(output, mode="w") as zoutput:
            for student in students:
                pdffile = _build_one_pdf(session, exam, student.login, False)
                if pdffile is None:
                    continue
                zoutput.writestr(
                    f"{semester_code}_{course_code}_{exam_slug}_{student.login}.pdf",
                    pdffile,
                )
                yield output.getvalue()

    yield output.getvalue()


@render.route("/pdfzip/<semester_code>_<course_code>_<exam_slug>.zip", methods=["GET"])
@auth.login_required
def build_pdfzip(semester_code, course_code, exam_slug, user=None, session=None):
    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

    return Response(
        gen_pdfzip(semester_code, course_code, exam_slug, session),
        mimetype="application/zip",
    )


@render.route(
    "/tables/<csvtype>/<semester_code>_<course_code>_<exam_slug>.<ext>", methods=["GET"]
)
@auth.login_required
def build_exam_table(
    csvtype, semester_code, course_code, exam_slug, ext, user=None, session=None
):
    if csvtype not in ("csv", "csv2", "xlsx"):
        abort(404)

    if csvtype.startswith("csv"):
        if ext != "csv":
            abort(404)
    else:
        if ext != "xlsx":
            abort(404)

    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exam = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .filter(models.Exam.slug == exam_slug)
            .first()
        )
        if exam is None:
            abort(404)

        _helpers.log_exam_action(session, user, exam, "Get exam table export")

        students, exam_results = _exam_helpers.get_exam_results(
            session, exam, course_semester, prefix_by_exam=False, papers=False
        )

        students.sort(
            key=lambda student: (
                _exam_helpers.sort_key_literal(student.surname),
                _exam_helpers.sort_key_literal(student.name),
                student.login,
            )
        )

        columns = ["Nom", "Prénom", "Login"]
        all_columns = columns + list(exam_results.keys())

        table = []

        for student in students:
            row = {
                "Nom": student.surname,
                "Prénom": student.name,
                "Login": student.login,
            }
            for column_name, column_results in exam_results.items():
                if csvtype.startswith("csv"):
                    val = column_results[student.login].displaygrade
                    if csvtype == "csv2":
                        val = val.replace(".", ",")
                else:
                    val = column_results[student.login].grade
                row[column_name] = val
            table.append(row)

    if csvtype.startswith("csv"):
        output = io.StringIO()
        csv_dialect = csv.excel
        if csvtype == "csv2":
            csv_dialect.delimiter = ";"
        csvwriter = csv.DictWriter(output, fieldnames=all_columns, dialect=csv.excel)
        csvwriter.writeheader()

        for row in table:
            csvwriter.writerow(row)

        response = make_response(output.getvalue().encode("utf8"))
        response.headers.set("Content-Type", "text/csv")
    else:
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.append(all_columns)
        for row in table:
            ws.append([row[c] for c in all_columns])
        output = io.BytesIO()
        wb.save(output)
        response = make_response(output.getvalue())
        response.headers.set(
            "Content-Type",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        )
    response.cache_control.max_age = 0
    return response


@render.route("/tables/<csvtype>/<semester_code>_<course_code>.<ext>", methods=["GET"])
@auth.login_required
def build_course_csv(csvtype, semester_code, course_code, ext, user=None, session=None):
    if csvtype not in ("csv", "csv2", "xlsx"):
        abort(404)

    if csvtype.startswith("csv"):
        if ext != "csv":
            abort(404)
    else:
        if ext != "xlsx":
            abort(404)

    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )
        _helpers.log_course_semester_action(
            session, user, course_semester, "Get coursetable export"
        )

        exams = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .order_by(models.Exam.date)
            .all()
        )

        if not exams:
            abort(404)

        exams_results = {}
        for exam in exams:
            students, exam_results = _exam_helpers.get_exam_results(
                session, exam, course_semester, papers=False, prefix_by_exam=True
            )
            exams_results.update(exam_results)

        students.sort(
            key=lambda student: (
                _exam_helpers.sort_key_literal(student.surname),
                _exam_helpers.sort_key_literal(student.name),
                student.login,
            )
        )

        columns = ["Nom", "Prénom", "Login", "Note"]
        all_columns = columns + list(exams_results.keys())

        table = []
        for student in students:
            row = {
                "Nom": student.surname,
                "Prénom": student.name,
                "Login": student.login,
                "Note": None,
            }
            for column_name, column_results in exams_results.items():
                if csvtype.startswith("csv"):
                    val = column_results[student.login].displaygrade
                    if csvtype == "csv2":
                        val = val.replace(".", ",")
                else:
                    val = column_results[student.login].grade
                row[column_name] = val
            table.append(row)

    if csvtype.startswith("csv"):
        output = io.StringIO()
        csv_dialect = csv.excel
        if csvtype == "csv2":
            csv_dialect.delimiter = ";"
        csvwriter = csv.DictWriter(output, fieldnames=all_columns, dialect=csv.excel)
        csvwriter.writeheader()
        for row in table:
            csvwriter.writerow(row)
        response = make_response(output.getvalue().encode("utf8"))
        response.headers.set("Content-Type", "text/csv")
    else:
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.append(all_columns)
        for row in table:
            ws.append([row[c] for c in all_columns])
        output = io.BytesIO()
        wb.save(output)
        response = make_response(output.getvalue())
        response.headers.set(
            "Content-Type",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        )
    response.cache_control.max_age = 0
    return response
