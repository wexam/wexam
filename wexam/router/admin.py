# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from flask import Blueprint, render_template, request, redirect, url_for

from ..auth import login_required, abort
from .. import models
from . import _helpers

admin = Blueprint(
    "admin",
    __name__,
    url_prefix="/admin",
    template_folder="templates",
    static_folder="static",
)


@admin.route("/manage/<semester>", methods=["GET"])
@admin.route("/")
@login_required
def manage(user=None, session=None, semester=None):
    with session.begin():
        if user.role != models.Role.admin:
            _helpers.log_forbidden(session, user)
            abort(403)
        semesters = (
            session.query(models.Semester)
            .order_by(models.Semester.start_date.desc())
            .all()
        )

        if semesters:
            if semester is None:
                semester = semesters[0]
            else:
                semester = (
                    session.query(models.Semester)
                    .filter(models.Semester.code == semester)
                    .first()
                )

                if semester is None:
                    abort(404)

            courses = session.query(models.Course).order_by(models.Course.code).all()
        else:
            courses = []

    return render_template(
        "admin.html",
        user=user,
        semester=semester,
        semesters=semesters,
        courses=courses,
        active=semester,
    )


@admin.route("/course/new", methods=["POST"])
@login_required
def course_new(user=None, session=None):
    with session.begin():
        if user.role != models.Role.admin:
            _helpers.log_forbidden(session, user)
            abort(403)
        course_code = request.form["code"]
        course_name = request.form["name"]

        _helper_course_new(
            session=session, course_code=course_code, course_name=course_name
        )

    return redirect(
        url_for(
            "admin.manage",
        )
    )


def _helper_course_new(session, course_code, course_name):
    if not course_name or not course_code:
        abort(422)

    if (
        session.query(models.Course).filter(models.Course.code == course_code).first()
        is not None
    ):
        abort(409)

    new_course = models.Course(code=course_code, name=course_name)
    session.add(new_course)


@admin.route("/semester/new", methods=["POST"])
@login_required
def semester_new(user=None, session=None):
    with session.begin():
        if user.role != models.Role.admin:
            _helpers.log_forbidden(session, user)
            abort(403)
        semester_code = request.form["code"]
        semester_name = request.form["name"]
        semester_begin = request.form["begin"]
        semester_end = request.form["end"]
        if (
            not semester_name
            or not semester_code
            or not semester_begin
            or not semester_end
        ):
            abort(422)

        if (
            session.query(models.Semester)
            .filter(models.Semester.code == semester_code)
            .first()
            is not None
        ):
            abort(409)

        new_semester = models.Semester(
            code=semester_code,
            name=semester_name,
            start_date=semester_begin,
            end_date=semester_end,
        )
        session.add(new_semester)
    return redirect(
        url_for(
            "admin.manage",
        )
    )


@admin.route("/semester/<semester_code>/new", methods=["POST"])
@login_required
def course_semester_new(semester_code, user=None, session=None):
    with session.begin():
        if user.role != models.Role.admin:
            _helpers.log_forbidden(session, user)
            abort(403)
        course_code = request.form["course_code"]
        if not course_code:
            abort(422)

        _helper_course_semester_new(
            session=session, semester_code=semester_code, course_code=course_code
        )

    return redirect(
        url_for(
            "admin.manage",
        )
    )


def _helper_course_semester_new(session, semester_code, course_code):
    semester = (
        session.query(models.Semester)
        .filter(models.Semester.code == semester_code)
        .first()
    )
    course = (
        session.query(models.Course).filter(models.Course.code == course_code).first()
    )

    if (
        session.query(models.CourseSemester)
        .filter(models.CourseSemester.course == course)
        .filter(models.CourseSemester.semester == semester)
        .first()
        is not None
    ):
        abort(409)

    new_course_semester = models.CourseSemester(course=course, semester=semester)
    session.add(new_course_semester)


@admin.route("/view/", methods=["GET"])
@admin.route("/view/<semester>", methods=["GET"])
@login_required
def view(semester=None, user=None, session=None):
    with session.begin():
        if user.role != models.Role.admin:
            _helpers.log_forbidden(session, user)
            abort(403)

        semesters = (
            session.query(models.Semester)
            .order_by(models.Semester.start_date.desc())
            .all()
        )

        _helpers.log_user_action(session, user, "Admin view")

        if semesters:
            if semester is None:
                semester = semesters[0]
            else:
                semester = (
                    session.query(models.Semester)
                    .filter(models.Semester.code == semester)
                    .first()
                )

                if semester is None:
                    abort(404)

            courses_semesters = (
                session.query(models.CourseSemester)
                .filter(models.CourseSemester.semester == semester)
                .order_by(models.CourseSemester.course_code)
                .all()
            )

            class FakeTeacher:
                role = models.Role.teacher

            courses_semesters_membership = [
                (cm, FakeTeacher()) for cm in courses_semesters
            ]
        else:
            courses_semesters_membership = []

    return render_template(
        "home.html",
        user=user,
        courses_semesters_membership=courses_semesters_membership,
        semesters=semesters,
        active=semester,
        status=_helpers.STATUS_FRONT,
        urlprefix="/admin/view/",
    )
